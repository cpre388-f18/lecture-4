package edu.iastate.jmay.lifecycle;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    TextView myTextView;

//    private int i = 0;
//    private static final String KEY_I = "i";

    // ViewModel
    private MainActivityViewModel iViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        if (savedInstanceState != null) {
//            i = savedInstanceState.getInt(KEY_I);
//        }

        // Locate or create ViewModel
        iViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);

        Log.i(TAG, "onCreate()");
        myTextView = findViewById(R.id.myTextView);
        updateTextView();
    }

//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//
//        outState.putInt(KEY_I, i);
//    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.i(TAG, "onDestroy()");
    }

    public void onButtonClick(View v) {
//        i++;
        iViewModel.incrementI();
        updateTextView();
    }

    private void updateTextView() {
//        myTextView.setText(Integer.toString(i));
        myTextView.setText(Integer.toString(iViewModel.getI()));
    }
}
