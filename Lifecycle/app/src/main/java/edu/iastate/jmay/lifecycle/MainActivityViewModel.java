package edu.iastate.jmay.lifecycle;

import android.arch.lifecycle.ViewModel;

// SEE build.grade (Module: app) for

public class MainActivityViewModel extends ViewModel {
    private int i = 0;

    public void incrementI() {
        i++;
    }

    public int getI() {
        return i;
    }
}
